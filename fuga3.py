
import random
import os
import subprocess
import threading
import keyboard #Using module keyboard

from os import listdir
from os.path import isfile, join




video_dir = os.getcwd() + '/videos'
video_paths = { 'frequente': [], 'timido': [], 'raro': [], }
for path, subdirs, files in os.walk(video_dir):
    for name in files:
        video_path = os.path.join(path, name)
#        print (video_path)

        s = video_path.split('/')
#        print (s)
        if ".mov" in s[-1]:
            if 'frequente' in s:
                video_paths["frequente"].append(video_path)
            elif "timido" in s:
                video_paths["timido"].append(video_path)
            elif "raro" in s:
                video_paths["raro"].append(video_path)



#print (video_paths)



def choose_video():
    perc = random.randint (1,100)
#    print ('perc', perc)

    if perc <= 20: 
        return random.choice(video_paths['raro'])
    elif perc <= 50:
        return random.choice(video_paths['timido'])
    else:
        return random.choice(video_paths['frequente'])




stop = False

def run_video():
    while not stop:
        flags = '  '


        total_length = 0

        for i in range(50):
            flags += ' --fs --autofit=100% --no-border --{ '


            # escolha do arquivo
            video = choose_video()
#            print ('arquivo', video)

            # split do diretório do vídeo
            videopath_s = video.split('/')

            #split do nome do arquivo
            videofile_s = videopath_s[-1]
            video_s = videofile_s.split('_')



            # duração
            if "C" in video_s:
                length = random.uniform(0.5,1.5) if random.random() < 0.6 else random.uniform(1.5,3.1)
                flags += ' --length=' + str(length)

            elif "M" in video_s:
                length = random.uniform(2.1,5.8)
                flags += ' --length=' + str(length)

            elif "L" in video_s:
                length = random.uniform(4.1,15)
                flags += ' --length=' + str(length)

            elif "T" in video_s:
                flags += ''
            
#            print ('durac', length)


            # velocidade
            if "v1" in video_s:
                speed = random.uniform(0.47,1.15)
                flags += ' --speed=' + str(speed)

            elif "v2" in video_s:
                speed = random.uniform(1,1.8)
                flags += ' --speed=' + str(speed)

            elif "v3" in video_s:
                speed = random.uniform(0.46,0.65) if random.random() < 0.45 else random.uniform(0.87,1.45)
                flags += ' --speed=' + str(speed)

            elif "v4" in video_s:
                speed = random.uniform(0.65,1) if random.random() < 0.5 else random.uniform(1,1.5)
                flags += ' --speed=' + str(speed)

            else:
                speed = random.uniform(0.46,1) if random.random() < 0.57 else random.uniform(1,1.8)
                flags += ' --speed=' + str(speed)

#            print ('vel', speed)

            # início
            if "T" in video_s:
                start = 0
                flags += ' --start=' + str(start) + '% '
            
            else:
                start = random.uniform(0,95)
                flags += ' --start=' +str(start) + '% ' 


            # corte preto
            cut_dur = random.uniform(0.1,0.9) if random.random() < 0.6 else random.uniform(0.9,2)


            flags += video + ' --} \--{ --length=' + str(cut_dur) + ' cut.mov --}'
            
        
        os.system('mpv ' + flags)
        print ('mpv ' + flags)



t = threading.Thread(target=run_video)
t.start()

while True:

    try:      #used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('esc'):      #if key 'esc' is pressed 
            stop = True
            break      #finishing the loop
        else:
            pass
    except:
        stop = True
        break #if user pressed a key other than the given key the loop will break




        # Separate in length group
# video_lengths = {"short": [], "long": []}

# for path in video_paths["main"]:
#     s = path.split('/')
#     if "long" in s:
#         video_paths["long"].append(video_path)
#     elif "short" in s:
#         video_paths["short"].append(video_path)
