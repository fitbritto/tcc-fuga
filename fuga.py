import random
import os
import subprocess
import threading
import keyboard #Using module keyboard

from os import listdir
from os.path import isfile, join


video_dir = os.getcwd() + '/videos'
video_paths = {'fade': {'black': [], 'white': []}, 'main': []}
for path, subdirs, files in os.walk(video_dir):
    for name in files:
        video_path = os.path.join(path, name)
#        print (video_path)

        s = video_path.split('/')
#        print (s)
        if ".mov" in s[-1]:
            if "fade" in s:
                if "black" in s:
                    video_paths["fade"]["black"].append(video_path)
                elif "white" in s:
                    video_paths["fade"]["white"].append(video_path)
            else:
                video_paths["main"].append(video_path)

# Separate in length group
# video_lengths = {"short": [], "long": []}

# for path in video_paths["main"]:
#     s = path.split('/')
#     if "long" in s:
#         video_paths["long"].append(video_path)
#     elif "short" in s:
#         video_paths["short"].append(video_path)



def choose_length():
    return random.uniform(0.7,10)

def choose_speed():
    return random.uniform(0.55,1) if random.random() < 0.5 else random.uniform(1,2.9)

def choose_start():
    return random.randrange(92)

def choose_video():
    video = random.choice(video_paths['main'])


    # Choose fade video
    video_s = video.split('/')

    fade = ""
    if "black" in video_s:
        fade = random.choice(video_paths["fade"]["black"])
    elif "white" in video_s:
        fade = random.choice(video_paths["fade"]["white"])

    return video, fade

stop = False

def run_video():
    while not stop:
        flags = '  '


        total_length = 0

        for i in range(50):
            flags += ' --{ '

            # speed
            speed = choose_speed()
            flags += ' --speed=' + str(speed)

            # start
            start = choose_start()
            flags += ' --start=' + str(start) + '%'

            # length
            length = choose_length()
            flags += ' --length=' + str(length)

            flags += ' --autofit=100% '

            # videofile
            video, fade = choose_video()
            flags += video
            # print ('arqv', f)

            # cut_to_black
            cut_dur = random.uniform(0.09,0.9) if random.random() < 0.5 else random.uniform(0.9,2)
            # print ('black', cut_dur)

            flags += ' --} --autofit=100% \--{ --length=' + str(cut_dur) + ' cut.mov --}'
            
        
        os.system('mpv --fs ' + flags)
       # print ('mpv --fs ' + flags)



# t = threading.Thread(target=run_video)
# t.start()

while True:

    try:      #used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('esc'):      #if key 'esc' is pressed 
            stop = True
            break      #finishing the loop
        else:
            pass
    except:
        stop = True
        break #if user pressed a key other than the given key the loop will break